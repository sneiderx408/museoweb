<!DOCTYPE html>
<html lang="zxx" class="no-js">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="../../../public/img/fav.png">
        <meta name="author" content="codepixer">
        <meta name="description" content="">
        <meta name="keywords" content="">
        <meta charset="UTF-8">
        <title>Art Museum</title>
        
        <link href="../../../public/css/bootstrap.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/css.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/linearicons.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/main.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/owl.carousel.css" rel="stylesheet" type="text/css"/>
        
        <link href="../../../public/css/theme/animate.min.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/theme/magnific-popup.css" rel="stylesheet" type="text/css"/>
        <link href="../../../public/css/theme/nice-select.css" rel="stylesheet" type="text/css"/>
        
    </head>
    <body>

        <?php
        require_once '../../../config/conn.php';
//        require_once '../controllers/tipoevento.class.php';
//        require_once '../controllers/division.class.php';
        require_once './header.php';

//        $tipoe = new tipoevento ();
//        $registostipoe = $tipoe->getTipoevento();
//
//        $division = new Division();
//         $registrodiv = $division->getDivision();
       
        
        ?>

        <!-- start banner Area -->
        <section class="banner-area relative" id="home">	
            <div class="overlay overlay-bg"></div>
            <div class="container">
                <div class="row d-flex align-items-center justify-content-center">
                    <div class="about-content col-lg-12">
                        <h1 class="text-white">
                            Registra Evento
                        </h1>	
                    </div>											
                </div>
            </div>
        </section>
        <!-- End banner Area -->	




        <!-- Start contact-page Area -->
        <section class="contact-page-area section-gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 d-flex flex-column address-wrap">
                        <div class="single-contact-address d-flex flex-row">

                        </div>


                    </div>
                    <div class="col-lg-8">

                        <form  id="frmSolicitud" name="frmSolicitud" action="evento.php" method="post" class="form-area contact-form text-right">
                            <div class="row">	
                                <div class="col-lg-6 form-group">

                                    <input id="nombreE" name="nombreE" placeholder="Nombre Evento"  onfocus="this.placeholder = ''" onblur="this.placeholder = 'Nombre Evento'" required="" class="common-input mb-20 form-control" type="text">
                                    
                                    <div class = "form-group">
                                        <div class = "col-xs-5 selectContainer">
                                            <select name = "tipoE" placeholder = "Color" onfocus = "this.placeholder = ''" onblur = "this.placeholder = 'Color'" required = "" class = "mb-20 form-control" type = "text" >
                                                <option value = "" >Tipo de evento</option>
                                                //<?php
//                                                foreach ($registostipoe as $index => $columna) {
//                                                    echo '<option value="' . $columna["codtipoe"] . '">' . $columna["nombretipoe"] . '</option>';
//                                                }
//                                                ?>
                                            </select>
                                        </div>
                                    </div>


                                    <input id="precioE" name="precioE" placeholder="Precio" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Precio'" class="common-input mb-20 form-control" required=""  type="number" min="0.00" max="10000.00" step="0.01">
                                    <div class="form-group">
                                        <div class="col-xs-5 selectContainer">
                                            <select name="divisionE" placeholder="Color"  onfocus="this.placeholder = ''" onblur="this.placeholder = 'Color'" required="" class="mb-20 form-control" type="text" >
                                                <option value="">Seleccione una división</option>
                                                //<?php
//                                                foreach ($registrodiv as $index => $columna) {
//                                                    echo '<option value="' . $columna["coddivision"] . '">' . $columna["nombredivision"] . '</option>';
//                                                }
//                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="mt-20 alert-msg" style="text-align: left;"></div>

                                    <div class="form-group">
                                        <input value="" type="date" name="dateE" max="3000-12-31" 
                                               min="1000-01-01" class="form-control">
                                    </div>


                                    <div class="form-group">
                                        <input type="time" name="timeE" max="3000-12-31" min="1000-01-01" class="form-control">
                                    </div>

                                    <button id="btnSave" type="submit"  class="primary-btn mt-20 text-white btn-block " style="float: right;">Guardar Evento</button>

                                </div>

                            </div>
                        </form>	

                    </div>
                </div>
            </div>	
        </section>




        <footer class="footer-area section-gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6>About Us</h6>
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore dolore magna aliqua.
                            </p>
                            <p class="footer-text">
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                                Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank">Colorlib</a>
                                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-5  col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6>Newsletter</h6>
                            <p>Stay update with our latest</p>
                            <div class="" id="mc_embed_signup">
                                <form target="_blank" novalidate="true" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="form-inline">
                                    <input class="form-control" name="EMAIL" placeholder="Enter Email" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Email '" required="" type="email">
                                    <button class="click-btn btn btn-default"><span class="lnr lnr-arrow-right"></span></button>
                                    <div style="position: absolute; left: -5000px;">
                                        <input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
                                    </div>

                                    <div class="info"></div>
                                </form>
                            </div>
                        </div>
                    </div>						
                    <div class="col-lg-2 col-md-6 col-sm-6 social-widget">
                        <div class="single-footer-widget">
                            <h6>Follow Us</h6>
                            <p>Let us be social</p>
                            <div class="footer-social d-flex align-items-center">
                                <a href="#"><i class="fa fa-facebook"></i></a>
                                <a href="#"><i class="fa fa-twitter"></i></a>
                                <a href="#"><i class="fa fa-dribbble"></i></a>
                                <a href="#"><i class="fa fa-behance"></i></a>
                            </div>
                        </div>
                    </div>							
                </div>
            </div>
        </footer>	
        <!-- End footer Area -->	

        <script src="../../../public/js/vendor/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="../../../public/js/vendor/bootstrap.min.js" type="text/javascript"></script>
        <script src="../../../public/js/js.js" type="text/javascript"></script>
        <script src="../../../public/js/imagesloaded.pkgd.min.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery.magnific-popup.min.js" type="text/javascript"></script>
        <script src="../../../public/js/jquery.validate.js" type="text/javascript"></script>
        <script src="../../../public/js/justified.min.js" type="text/javascript"></script>
        <script src="../../../public/js/mail-script.js" type="text/javascript"></script>
        <script src="../../../public/js/main.js" type="text/javascript"></script>
        <script src="../../../public/js/owl.carousel.min.js" type="text/javascript"></script>
        <script src="../../../public/js/superfish.js" type="text/javascript"></script>

        <script src="../../../public/js/theme/additional-methods.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/easing.min.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/hoverIntent.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/jquery.ajaxchimp.min.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/jquery.nice-select.min.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/jquery.sticky.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/parallax.min.js" type="text/javascript"></script>
        <script src="../../../public/js/theme/popper.js" type="text/javascript" ntegrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>

        <script>


                                        $("#frmSolicitud").validate(
                                                ({
                                                    rules: {

                                                        email: {
                                                            required: true,
                                                            email: true,
                                                            minlength: 8,
                                                            maxlength: 40
                                                        },
                                                        password: {
                                                            required: true
                                                        }

                                                    },
                                                    messages: {

                                                        email: {
                                                            required: "Este campos es requerido"
                                                        },
                                                        password: {
                                                            required: "Este campos es requerido"
                                                        }
                                                    }

                                                });
                                                );




        </script>
    </body>
</html>



