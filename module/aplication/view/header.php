
<header id="header" id="home">
    <div class="container header-top">
        <div class="row">
            <div class="col-6 top-head-left">
                <ul>
                    <li><a href="#">Visit Us</a></li>
                    <li><a href="#">Buy Ticket</a></li>
                </ul>
            </div>
            <div class="col-6 top-head-right">
                <ul>
                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                    <li><a href="login.php"> Administrador </a></li>
                </ul>
            </div>			  			
        </div>
    </div>
    <hr>
    <div class="container">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="index.php"><img src="../img/logo.png" alt="" title="" /></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="index.php">Home</a></li>
                    <li><a href="about.php">Informacion</a></li>
                    <li><a href="gallery.php">Galeria</a></li>
                    <li><a href="event.php">Eventos</a></li>
                    <li><a href="ticket.php">Tickets</a></li>
                    <li><a href="contact.php">Contacto</a></li>

            </nav>		    		
        </div>
    </div>
</header>

